function getRegions(){
    let xmlhttp = new XMLHttpRequest();
    let url = "http://dataservice.accuweather.com/locations/v1/regions?apikey=VsIlohTTjWRkXzAGVHsDx54olAmNBWOf";

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let myArr = JSON.parse(this.responseText);
            fetchRegions(myArr);
            console.log(myArr);
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function fetchRegions(arr) {
    let out = "<div style='max-width: 600px; margin: auto;'><span style= ' border: ridge; border-radius: 6px; display: flex; justify-content: space-around; padding: 20px; background-color: #fcded9;'><span>Region ID</span> <span>Region Name</span></span>";
    arr.map(state=>{
        out+="<span style='margin: 10px; background-color: #e5efd2; border: ridge; border-radius: 6px; display: flex; justify-content: space-around; padding: 20px'><span>"+state.ID+"</span><span>"+state.LocalizedName+"</span></span>";
    });
    out+= "</div>";
    document.getElementById("id01").innerHTML = out;
}

function fetchCountries(){
    const form = document.getElementById('searchRegion');
    const region = form.elements.region.value;

    if (region == "") {
        alert("Region must be filled out");
        return false;
    }else{
        let xmlhttp = new XMLHttpRequest();
        let url = `http://dataservice.accuweather.com/locations/v1/countries/${region}?apikey=VsIlohTTjWRkXzAGVHsDx54olAmNBWOf`;

        xmlhttp.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                let myArr = JSON.parse(this.responseText);
                getCountries(myArr);
            }
        };
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }
}

function getCountries(arr){
    let out = "<div style='max-width: 600px; margin: auto;'><span style= ' border: ridge; border-radius: 6px; display: flex; justify-content: space-around; padding: 20px; background-color: #fcded9;'><span>Country ID</span> <span>Country Name</span></span>";;
    arr.map(country=>{
        out+="<span style='margin: 10px; background-color: #e5efd2; border: ridge; border-radius: 6px; display: flex; justify-content: space-around; padding: 20px'><span>"+country.ID+"</span><span>"+country.LocalizedName+"</span></span>";;
    });
    out+= "</div>";
    document.getElementById("id01").innerHTML = out;
}