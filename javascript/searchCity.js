function fetchCityKey(){
    const form = document.getElementById('searchCity');
    const city = form.elements.city.value;

    let xmlhttp = new XMLHttpRequest();
    let url = `http://dataservice.accuweather.com/locations/v1/cities/search?apikey=VsIlohTTjWRkXzAGVHsDx54olAmNBWOf&q=${city}`;

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let myArr = JSON.parse(this.responseText);
            getWeather(city, myArr[0].Key)
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function getWeather(city, key){
    let xmlhttp = new XMLHttpRequest();
    let url = `http://dataservice.accuweather.com/currentconditions/v1/${key}?apikey=VsIlohTTjWRkXzAGVHsDx54olAmNBWOf`;

    xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            let myArr = JSON.parse(this.responseText);
            displayInformation(city, myArr)
        }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();
}

function displayInformation(cityName, arr){
    let out = "<div style='max-width: 600px; margin: auto;'><span style= ' border: ridge; border-radius: 6px; display: flex; justify-content: space-around; padding: 20px; background-color: #fcded9;'><span>City name</span><span>Temp</span> <span>How it looks</span></span>";;
    arr.map(city=>{
        out+="<span style='margin: 10px; background-color: #e5efd2; border: ridge; border-radius: 6px; display: flex; justify-content: space-around; padding: 20px'><span>"+cityName+"</span><span>"+`${city.Temperature.Metric.Value} Celcius`+"</span><span>"+city.WeatherText+"</span></span>";;
    });
    out+= "</div>";
    document.getElementById("id01").innerHTML = out;
}